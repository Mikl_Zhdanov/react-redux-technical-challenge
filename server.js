const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const boatRamps = require('./data/boat_ramps.json');
const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.get('/boat-ramps', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(boatRamps);
});

app.listen(process.env.PORT || 8080);