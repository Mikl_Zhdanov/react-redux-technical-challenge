import React, { Component } from 'react';
import 'babel-polyfill';
import { connect } from 'react-redux';
import { Route, Redirect, withRouter, Switch, NavLink } from 'react-router-dom';
import BoatRamps from './pages/boatRamps';
import SomePage from './pages/somePage';
import './style.css';

class App extends Component {
  handleClick() {}

  render() {
    const { data } = this.props;
    return (
      <div className="app">
        <div className="app-nav">
          <NavLink to="/map" activeClassName="active">
            Map
          </NavLink>
          <NavLink to="/somePage" activeClassName="active">
            somePage
          </NavLink>
        </div>
        <Switch>
          <Route exact path="/map" component={BoatRamps} />
          <Route exact path="/somePage" component={SomePage} />
          <Redirect from="*" to="/map" />
        </Switch>
        {data}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({});

export default withRouter(
  connect(
    mapStateToProps,
    {}
  )(App)
);
