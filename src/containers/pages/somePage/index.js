import React, { Component } from 'react';
import { connect } from 'react-redux';

class SomePage extends Component {
  render() {
    return <div className="page">some page goes here</div>;
  }
}

const mapStateToProps = (state, ownProps) => ({});

export default connect(
  mapStateToProps,
  {}
)(SomePage);
