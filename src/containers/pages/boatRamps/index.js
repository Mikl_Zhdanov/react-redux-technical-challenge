// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Filter from '../../../components/filter';
import Gmap from '../../../components/gmap';

import './style.css';

type P = {
  map: Object
};

type S = {
  boatRamps: Array<Object>
};

class Map extends Component<P, S> {
  state = {
    boatRamps: undefined
  };

  showResults = results => {
    this.setState({ boatRamps: results });
  };

  render() {
    const { map } = this.props;

    const { boatRamps } = this.state;

    const boatRampsToShow = boatRamps ? boatRamps : map.boatRamps;

    return (
      <div className="page page-map">
        {boatRampsToShow ? (
          <React.Fragment>
            <Filter boatRamps={map.boatRamps} showResults={this.showResults} />
            <Gmap data={boatRampsToShow} />
          </React.Fragment>
        ) : map.error ? (
          <span>Connection error</span>
        ) : (
          <span>loading...</span>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  map: state.map
});

export default connect(
  mapStateToProps,
  {}
)(Map);
