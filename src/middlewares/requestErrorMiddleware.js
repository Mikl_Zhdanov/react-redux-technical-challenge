import { RSAA } from 'redux-api-middleware';

export default store => next => action => {
  if (action.error && action.type.indexOf('_REQUEST') !== -1) {
    const apiCallAction = action[RSAA];
    const requestActionType = action.type;
    const errorActionType =
      requestActionType.substring(0, requestActionType.indexOf('_REQUEST')) +
      '_FAILURE';
    const errorAction = {
      ...apiCallAction,
      type: errorActionType
    };
    return next(errorAction);
  } else {
    return next(action);
  }
};
