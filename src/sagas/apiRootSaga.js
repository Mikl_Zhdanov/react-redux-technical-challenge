import { put, take } from 'redux-saga/effects';
import { loadConfig, LOAD_CONFIG_SUCCESS } from '../actions/config';

export default function* apiRoot() {
  yield put(loadConfig());
  yield take(LOAD_CONFIG_SUCCESS);
}
