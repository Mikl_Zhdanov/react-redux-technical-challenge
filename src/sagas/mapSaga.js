import { put, take, call } from 'redux-saga/effects';
import {
  getBoatRamps,
  GET_BOAT_RAMPS_SUCCESS,
  GET_BOAT_RAMPS_PRETTIFIRE
} from '../actions/map';

const getCoodObject = coordArr => ({ lng: coordArr[0], lat: coordArr[1] });

function* mapSaga() {
  yield put(getBoatRamps());
  const boatRoams = yield take(GET_BOAT_RAMPS_SUCCESS);

  const boatRoamsPrettified = boatRoams.payload.features.map(feature => {
    let coords = feature.geometry.coordinates[0][0];
    coords = coords.map(coordArr => getCoodObject(coordArr));
    return {
      ...feature,
      geometry: { ...feature.geometry, coordinates: coords }
    };
  });

  yield put({ type: GET_BOAT_RAMPS_PRETTIFIRE, payload: boatRoamsPrettified });
}

export default mapSaga;
