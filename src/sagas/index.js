import { fork, all, call } from 'redux-saga/effects';
import { router } from 'redux-saga-router';
import apiRootSaga from './apiRootSaga';
import mapSaga from './mapSaga';

const createRootSaga = (history, dispatch) => {
  const routes = {
    '/map': function* mainPageSaga() {
      yield fork(mapSaga);
    }
  };
  return function* routesSaga() {
    yield fork(router, history, routes);
  };
};

export default function* root(history, dispatch) {
  yield call(apiRootSaga);
  yield all([fork(createRootSaga(history, dispatch))]);
}
