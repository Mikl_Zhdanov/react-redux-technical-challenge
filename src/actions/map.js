import { RSAA } from 'redux-api-middleware';

export const GET_BOAT_RAMPS_REQUEST = 'GET_BOAT_RAMPS_REQUEST';
export const GET_BOAT_RAMPS_SUCCESS = 'GET_BOAT_RAMPS_SUCCESS';
export const GET_BOAT_RAMPS_FAILURE = 'GET_BOAT_RAMPS_FAILURE';

export const GET_BOAT_RAMPS_PRETTIFIRE = 'GET_BOAT_RAMPS_PRETTIFIRE';

export const getBoatRamps = () => ({
  [RSAA]: {
    endpoint: 'boat-ramps',
    method: 'GET',
    types: [
      GET_BOAT_RAMPS_REQUEST,
      GET_BOAT_RAMPS_SUCCESS,
      GET_BOAT_RAMPS_FAILURE
    ]
  }
});
