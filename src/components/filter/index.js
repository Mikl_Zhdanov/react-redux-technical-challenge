//@flow
import React, { Component } from 'react';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';

import './style.css';

type P = {
  boatRamps: Array<Object>,
  showResults: Function
};

type S = {
  rangeValue: Object,
  boatRamps: Array<Object>
};

class Filter extends Component<P, S> {
  state = {
    rangeValue: { min: 0, max: 550 },
    boatRamps: undefined
  };

  componentWillMount() {
    const { boatRamps } = this.props;
    this.setState({ boatRamps: boatRamps });
  }

  filterBoatRamps = (value: Object) => {
    const { boatRamps } = this.props;
    let filteredBoatRamps = boatRamps.filter(
      roam =>
        roam.properties.area_ >= value.min && roam.properties.area_ <= value.max
    );

    if (value.max <= 550 && value.min >= 0) {
      this.setState({ rangeValue: value, boatRamps: [...filteredBoatRamps] });
    }
  };

  showResults = () => {
    const { showResults } = this.props;
    showResults(this.state.boatRamps);
  };

  render() {
    const { boatRamps, rangeValue } = this.state;

    const resultsEnum = boatRamps && boatRamps.length;

    return (
      <div className="filters-block">
        <h4>Area range</h4>
        <InputRange
          maxValue={550}
          minValue={0}
          step={50}
          value={rangeValue}
          onChange={value => {
            this.filterBoatRamps(value);
          }}
        />
        {resultsEnum ? (
          <div className="filters-block-results" onClick={this.showResults}>
            {`Show ${resultsEnum} results`}
          </div>
        ) : null}
      </div>
    );
  }
}

export default Filter;
