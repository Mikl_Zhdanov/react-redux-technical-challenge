// @flow
import React, { Component } from 'react';
import { compose, withProps } from 'recompose';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Polygon,
  InfoWindow
} from 'react-google-maps';
import './style.css';

type P = {
  data: Array<Object>
};

type S = {
  infoWindowOpened: boolean,
  infoWindow: Object
};

class Gmap extends Component<P, S> {
  state = {
    infoWindowOpened: false,
    infoWindow: {
      position: undefined,
      properties: undefined
    }
  };

  showInfo({ lat, lng, properties }) {
    this.setState({
      infoWindowOpened: true,
      infoWindow: {
        position: { lat, lng },
        properties
      }
    });
  }

  render() {
    const { data } = this.props;
    const {
      infoWindowOpened,
      infoWindow: { position, properties }
    } = this.state;

    const polygonsArr = data.map(feature => {
      let coords = feature.geometry.coordinates;

      return (
        <Polygon
          path={coords}
          key={feature.id}
          onClick={e =>
            this.showInfo({
              lat: e.latLng.lat(),
              lng: e.latLng.lng(),
              properties: feature.properties
            })
          }
          options={{
            fillColor: '#3498db',
            fillOpacity: 0.4,
            strokeColor: '#2980b9',
            strokeOpacity: 1,
            strokeWeight: 5
          }}
        />
      );
    });

    return (
      <GoogleMap
        defaultZoom={10}
        defaultCenter={{ lng: 153.42471311408283, lat: -27.99222816447195 }}
      >
        {polygonsArr}
        {infoWindowOpened ? (
          <InfoWindow
            onCloseClick={e => {
              console.log(e);
              this.setState({ infoWindowOpened: false });
            }}
            position={position}
          >
            <ul>
              <li>type: {properties.type}</li>
              <li>area: {properties.area_}</li>
              <li>owner: {properties.owner}</li>
              <li>material: {properties.material}</li>
              <li>asset_numb: {properties.asset_numb}</li>
            </ul>
          </InfoWindow>
        ) : null}
      </GoogleMap>
    );
  }
}

export default compose(
  withProps({
    googleMapURL:
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyDf-yIqxErTkbWzKhLox7nAANnrfDIY190',
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)(Gmap);
