import * as ConfigActions from '../actions/config';

const initialState = {
  apiRoot: null
};

export const config = (state = initialState, action) => {
  const { type, payload } = action;

  if (type === ConfigActions.LOAD_CONFIG_SUCCESS) {
    const { apiRoot } = payload;
    return { apiRoot };
  }

  return state;
};
