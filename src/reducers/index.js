import { map } from './map';
import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import { config } from './config';

const rootReducer = combineReducers({
  routing,
  config,
  map
});

export default rootReducer;
