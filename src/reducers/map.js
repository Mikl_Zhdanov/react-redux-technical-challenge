import {
  GET_BOAT_RAMPS_REQUEST,
  GET_BOAT_RAMPS_PRETTIFIRE,
  GET_BOAT_RAMPS_FAILURE
} from '../actions/map';

const initialState = {
  boatRamps: undefined,
  loading: false,
  error: undefined
};

export const map = (state = initialState, action) => {
  const { type } = action;

  switch (type) {
    case GET_BOAT_RAMPS_REQUEST:
      return { ...state, loading: true, error: undefined };
    case GET_BOAT_RAMPS_PRETTIFIRE:
      return { ...state, loading: false, boatRamps: action.payload };
    case GET_BOAT_RAMPS_FAILURE:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
