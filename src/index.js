import React from 'react';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { render } from 'react-dom';
import createHistory from 'history/createBrowserHistory';
import Root from './containers/root';
import configureStore from './store';
import saga from './sagas/index';

const history = createHistory();
const store = configureStore(history);
store.runSaga(saga, history, store.dispatch);

render(
  <Root store={store} history={history} />,
  document.getElementById('root')
);

registerServiceWorker();
