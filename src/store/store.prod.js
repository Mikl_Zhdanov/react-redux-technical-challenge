import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { apiMiddleware } from 'redux-api-middleware';
import persistState from 'redux-localstorage';
import createSagaMiddleware from 'redux-saga';
import asyncAwait from 'redux-async-await';
import { apiRoot } from '../middlewares';
import { requestErrorMiddleware } from '../middlewares';
import rootReducer from '../reducers';

const configureStore = history => {
  const sagaMiddleware = createSagaMiddleware();
  const router = routerMiddleware(history);
  const store = createStore(
    rootReducer,
    compose(
      applyMiddleware(
        apiRoot,
        asyncAwait,
        apiMiddleware,
        requestErrorMiddleware,
        sagaMiddleware,
        router
      ),
      persistState(['config'])
    )
  );

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  store.runSaga = sagaMiddleware.run;

  return store;
};

export default configureStore;
