# React/Redux Technical challenge

To install this project:

```
npm install
```

Start express server before run frontend part:

```
node server.js
```

For development run:

```
npm start
```

or use yarn instead npm